import os
from setuptools import find_packages, setup

README = 'Coming Soon'

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='vault-client',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    description='Package provides API to make http requests to the tedvault api',
    long_description=README,
    url='gitlab.com',
    author='Nana Duah',
    install_requires=[
        'requests',
    ],
)